# Lunni

<dt>lunni</dt>
<dd>(Finnish) puffin (any seabird of the genus Fratercula of the auk family Alcidae)</dd>

Lunni is a simple personal PaaS / hosting. You can easily host some apps for yourself, your friends or your small business. It is easy to install and is based on industry standard technologies: Docker Swarm, Traefik, and Portainer.

To get started,

```sh
wget https://lunni.ale.sh/setup.sh  # grab the script
less setup.sh                       # inspect it
bash setup.sh                       # run it
```

If everything went alright, you should now have a working Docker Swarm “cluster“. It would consist of one node only, but you can extend it if you outgrow it. Now it's time to [deploy something there](./deploy.md)!
