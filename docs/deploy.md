Lunni is easy to use if you have even just a little experience with Docker and docker-compose. You can easily use the same `docker-compose.yml` for development on your local machine and deployment with Lunni.

You should now have a working swarm and an admin panel running at `portainer.<your-hostname>` – let's take a look:

> Portainer home screen.png

Portainer tries to be kind of a one-size-fits-all thing, so it can manage dozens of clusters (called “endpoints” here), but we only have one – `default`. Let's go there and take a look at **Stacks** we have.

> Stacks.webp

If you're just getting started, you'll see only one stack here: `lunni`. That's the group of services that powers Lunni. Let's add another one, an actual application. For now let's grab something available at Docker Hub – [Gitea](https://hub.docker.com/r/gitea/gitea) might be a good example. Click the **Add stack** button:

> addstack.exe

Now you'll have to choose a name and paste your docker-compose file. Luckily Gitea authors [provide some examples](https://docs.gitea.io/en-us/install-with-docker/) for us already (adapted a bit for brevity):

```yaml
version: "2"

services:
  server:
    image: gitea/gitea:latest
    environment:
      - USER_UID=1000
      - USER_GID=1000
      - DB_TYPE=postgres
      - DB_HOST=db:5432
      - DB_NAME=gitea
      - DB_USER=gitea
      - DB_PASSWD=gitea
    restart: always
    volumes:
      - ./gitea:/data
      - /etc/timezone:/etc/timezone:ro
      - /etc/localtime:/etc/localtime:ro
    ports:
      - "3000:3000"
      - "222:22"
    depends_on:
      - db

  db:
    image: postgres:9.6
    restart: always
    environment:
      - POSTGRES_USER=gitea
      - POSTGRES_PASSWORD=gitea
      - POSTGRES_DB=gitea
    volumes:
      - ./postgres:/var/lib/postgresql/data
```

We'll need to add a couple more options though for it to work with Lunni:

- update Compose `version` to `3`,

- define some networks and volumes:

  ```yaml
  volumes:
    gitea-data:
    db-data:

  networks:
    default:
    traefik-public:
      external: true
  ```

- add `networks: [default, traefik-public]` to the `web` service so our reverse proxy can see it,

- add `networks: [default]` to all other services (databases, message queues, mail servers – everything not public),

- change  volume from absolute path to the named volumes we created,

- add some boilerplate labels to the `web` service so that we know where to publish it and how:

  ```yaml
  deploy:
    labels:
      - traefik.enable=true

      # Hostname you want your app to appear at:
      - traefik.frontend.rule=Host:${DOMAIN?Variable DOMAIN not set}

      # Port your app runs on inside container:
      - traefik.port=3000

      # Some boilerplate you should probably copy as is unless you know
      # what you're doing:
      - traefik.tags=traefik-public
      - traefik.docker.network=traefik-public
      - traefik.redirectorservice.frontend.entryPoints=http
      - traefik.redirectorservice.frontend.redirect.entryPoint=https
      - traefik.webservice.frontend.entryPoints=https
  ```

- and pass the `DOMAIN` environment variable to Gitea.

You should end up with something like this:

```yaml
version: "3"
services:
  web:
    image: gitea/gitea:latest
    environment:
      - USER_UID=1000
      - USER_GID=1000
      - DB_TYPE=postgres
      - DB_HOST=db:5432
      - DB_NAME=gitea
      - DB_USER=gitea
      - DB_PASSWD=gitea
      - DOMAIN=${DOMAIN?Variable DOMAIN not set}
    volumes:
      - gitea-data:/data
    networks: [default, traefik-public]
    expose:
      - "3000"
    ports:
      - "22222:22"
    depends_on:
      - db
    deploy: 
      labels:
        - traefik.enable=true
        - traefik.frontend.rule=Host:${DOMAIN?Variable DOMAIN not set}
        - traefik.port=3000
        - traefik.tags=traefik-public
        - traefik.docker.network=traefik-public
        - traefik.redirectorservice.frontend.entryPoints=http
        - traefik.redirectorservice.frontend.redirect.entryPoint=https
        - traefik.webservice.frontend.entryPoints=https

  db:
    image: postgres:9.6
    restart: always
    environment:
      - POSTGRES_USER=gitea
      - POSTGRES_PASSWORD=gitea
      - POSTGRES_DB=gitea
    volumes:
      - db-data:/var/lib/postgresql/data
    networks: [default]

volumes:
  gitea-data:
  db-data:

networks:
  default:
  traefik-public:
    external: true
```

Copy-paste this to Portainer, click **Add environment variable** and add `DOMAIN=git.<your-hostname>`.

After saving go to `https://git.<your-hostname>`. For the first couple of minutes you'll get certificate warnings – this means Lunni is still requesting the certificate from Let's Encrypt.

Congrats on deploying your first application! 🎉
